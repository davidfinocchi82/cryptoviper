var http = require('http');

var request = 'test';

var options = {
  socketPath: '/tmp/tester',
  path: '/',
  method: 'POST',
  headers: {
    'Content-Length': request.length
  }
};

var req = http.request(options, function(res){
  var data = ''
  
  res.on('data', function (chunk) {
    data += chunk;
  });
  
  res.on('end', function() {
    console.log('DATA SENT:', data);
  });
});

req.on('error', function(error) {
  console.error('ERROR:',error);
});

req.write(request);
req.end();

var net = require('net');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var jsonParser = bodyParser.json();

var socketSend = function(request) {
  var client = new net.Socket();

  return new Promise((resolve, reject) => {
    let data = '';
    
    client.connect('/tmp/david', () => {
      console.log('CONNECTED...');
      client.write(request);
      client.destroy();
    });

    client.on('data', (chunk) => {
      data += chunk;
    });

    client.on('close', () => {
      resolve(`DATA SENT TO BOT:\n${request}\n`);
    });
  });
}

app.get('/',function(req,res){
    res.send('Hello world');
    console.log('GET REQUEST:');
    console.log(req.rawHeaders);
});

app.post('/', jsonParser, function(req,res){
  socketSend(JSON.stringify(req.body))
    .then((res) => console.log(res))
    .then(() => res.send('OK'))
    .catch((error) => console.error(error));
});

var server = app.listen(3000,function(){
    console.log("Signal webhook starting on port 3000");
});


"""Poloniex trade thread."""
import configparser
import time
import threading
from datetime import datetime, timedelta
from poloniex import Poloniex

class PoloniexThread:
    """Docstring."""
    def __init__(self, coin, pair):
        self.config = configparser.ConfigParser()
        self.config.read("config.ini")
        self.exchange = Poloniex(self.config.get('poloniex', 'key'),
                                 self.config.get('poloniex', 'secret'))
        self.coin = coin
        self.pair = pair
        self.total_btc = round(float(self.exchange.returnBalances()['BTC']), 8)
        self.quantity = self.buy_quantity()
        self.stoploss = 0
        self.take_profit = 0
        self.trailing_stop = 0
        self.purchase_time = datetime.now()
        self.buy_completed = False
        self.sell_completed = False
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def get_buy_market(self):
        """Docstring."""
        while True:
            try:
                ticks = self.exchange.returnTicker()
                break
            except:
                time.sleep(5)
        return float(ticks[self.pair]['lowestAsk'])

    def get_sell_market(self):
        """Docstring."""
        while True:
            try:
                ticks = self.exchange.returnTicker()
                break
            except:
                time.sleep(5)
        return float(ticks[self.pair]['highestBid'])

    def buy_quantity(self):
        """Docstring."""
        return round(self.trade_amount() / self.get_buy_market(), 8)

    def buy(self):
        """Docstring."""
        return self.exchange.buy(self.pair, self.get_buy_market(), self.quantity)

    def sell(self):
        """Docstring."""
        return self.exchange.sell(self.pair, self.get_sell_market(), self.sell_quantity)

    def trade_amount(self):
        """Docstring."""
        return round(self.total_btc * self.config.getfloat('general', 'TRADE_PERCENTAGE'), 8)

    def sell_amount(self):
        """Docstring."""
        return self.quantity - (self.quantity * float(self.config.get('poloniex', 'fee')))

    def progress(self, current_bid):
        """Docstring."""
        win_loss = round((current_bid - self.buy_order_rate) / self.buy_order_rate * 100, 2)
        print("======================================")
        print("=> WAITING FOR SELL SINCE : " + str(self.purchase_time))
        print("=> BUY: " + self.pair + " @ Poloniex")
        print("=> BUY PRICE: ", format(self.buy_order_rate, '.8f'))
        print("=> Win/Loss Percent: " + str(format(win_loss, '.2f')) + '%')
        print("=> Current Stop Loss: ", format(self.stoploss, '.8f'))
        print("======================================")

    def await_buy(self):
        """Docstring."""
        count = 0
        while True:
            open_order = self.exchange.returnOpenOrders(self.pair)
            if not open_order:
                print("Bought " + self.coin + " on Poloniex")
                self.buy_completed = True
                self.purchase_time = datetime.now()
                break
            if count > self.config.getint('general', 'TIME_FOR_BUY_CANCEL'):
                buy_closed = int(self.exchange.cancelOrder(self.buy_order_id)['success'])
                if buy_closed == 1:
                    break
            count = count + 1
            time.sleep(1)

    def await_sell(self):
        """Docstring."""
        count = 0
        while True:
            open_order = self.exchange.returnOpenOrders(self.pair)
            if not open_order:
                self.sell_completed = True
                break
            if count > self.config.getint('general', 'TIME_FOR_SELL_CANCEL'):
                sell_closed = int(self.exchange.cancelOrder(self.sell_order_id)['success'])
                if sell_closed == 1:
                    break
            count = count + 1
            time.sleep(1)

    def run(self):
        """Docstring."""
        while not self.buy_completed:
            self.buy_order = self.buy()
            self.buy_order_id = self.buy_order["orderNumber"]
            self.buy_order_rate = self.get_buy_market()
            self.sell_order_rate = self.get_sell_market()
            if self.buy_order_rate < self.sell_order_rate:
                self.spread = round(self.sell_order_rate - self.buy_order_rate, 8)
            elif self.buy_order_rate > self.sell_order_rate:
                self.spread = round(self.buy_order_rate - self.sell_order_rate, 8)
            else:
                self.spread = 0
            if not self.config.getboolean('general', 'USE_TRAILING'):
                self.take_profit = round(self.sell_order_rate + (self.sell_order_rate * self.config.getfloat('general', 'PROFITMARGIN')), 8) + self.spread
            self.sell_quantity = round(self.sell_amount(), 8)
            self.await_buy()
            #float(self.buy_order['resultingTrades'][0]['rate'])
        if self.buy_completed and not self.sell_completed:
            i = 0
            while not self.sell_completed:
                current_bid = self.get_sell_market()
                if i >= 300:
                    self.progress(current_bid)
                    i = 0
                if self.trailing_stop == 0 and self.config.getboolean('general', 'USE_TRAILING'):
                    self.trailing_stop = round(self.sell_order_rate + (self.sell_order_rate * self.config.getfloat('general', 'TRAIL_PERCENT')), 8) + self.spread
                if current_bid >= self.trailing_stop and self.config.getboolean('general', 'USE_TRAILING'):
                    self.stoploss = round(current_bid - (current_bid * self.config.getfloat('general', 'TRAIL_STEP')), 8)
                    self.trailing_stop = round(self.sell_order_rate + (self.sell_order_rate * self.config.getfloat('general', 'TRAIL_PERCENT')), 8)
                if self.stoploss >= current_bid or datetime.now() >= (self.purchase_time + timedelta(hours=self.config.getint('general', 'CLOSE_TIME'))) or (current_bid >= self.take_profit and not self.config.getboolean('general', 'USE_TRAILING')):
                    self.sell_order = self.sell()
                    print("Sold " + self.coin + ' on Poloniex')
                    self.sell_order_id = self.sell_order["orderNumber"]
                    self.sell_completed = True
                i += 1
                time.sleep(1)

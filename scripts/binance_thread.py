import configparser
import time
import threading
import numpy as np
import decimal
from datetime import datetime, timedelta
import main
import psycopg2
from binance.client import Client
from binance.websockets import BinanceSocketManager

class BinanceThread:
    """Docstring."""
    def __init__(self, coin, pair):
        self.config = configparser.ConfigParser()
        self.config.read("config.ini")
        self.exchange = Client(self.config.get('binance', 'key'), self.config.get('binance', 'secret'))
        self.coin = coin
        self.pair = pair
        self.total_btc = 0
        self.quantity = 0
        self.stoploss = 0
        self.take_profit = 0
        self.trailing_stop = 0
        self.purchase_time = datetime.now()
        self.buy_completed = False
        self.sell_completed = False
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def get_buy_market(self):
        """Docstring."""
        while True:
            ticks = self.exchange.get_orderbook_ticker(symbol=self.pair)
            if ticks['symbol'] == self.pair:
                break
            else:
                time.sleep(5)
        return float(ticks['askPrice'])

    def get_sell_market(self):
        """Docstring."""
        while True:
            ticks = self.exchange.get_orderbook_ticker(symbol=self.pair)
            if ticks['symbol'] == self.pair:
                break
            else:
                time.sleep(5)
        return float(ticks['bidPrice'])

    def buy_quantity(self):
        """Docstring."""
        return round(self.trade_amount() / self.get_buy_market(), self.lot)

    def buy(self):
        """Docstring."""
        return self.exchange.create_order(symbol=self.pair, side=self.exchange.SIDE_BUY, type=self.exchange.ORDER_TYPE_MARKET , quantity=self.quantity)

    def sell(self):
        """Docstring."""
        return self.exchange.create_order(symbol=self.pair, side=self.exchange.SIDE_SELL, type=self.exchange.ORDER_TYPE_MARKET , quantity=self.sell_amount())

    def trade_amount(self):
        """Docstring."""
        return round(self.total_btc * self.config.getfloat('general', 'TRADE_PERCENTAGE'), 8)

    def sell_amount(self):
        """Docstring."""
        result = self.exchange.get_account()
        for res in result['balances']:
            if res['asset'] == self.coin:
                amount = float(res['free'])
        return float(self.truncate(amount, self.lot))

    def progress(self, current_bid):
        """Docstring."""
        win_loss = round((current_bid - self.sell_order_rate) / self.sell_order_rate * 100, 3)
        print("======================================")
        print("=> WAITING FOR SELL SINCE : " + str(self.purchase_time))
        print("=> BUY: " + self.pair + " @ Binance")
        print("=> Buy Price: ", format(self.buy_order_rate, '.8f'))
        print("=> Current Price: ", format(current_bid, '.8f'))
        print("=> Take Profit: ", format(self.take_profit, '.8f'))
        print("=> Stop Loss: ", format(self.stoploss, '.8f'))
        print("=> Win/Loss Percent: " + str(format(win_loss, '.2f')) + '%')
        print("======================================")

    def get_btc_bal(self):
        result = self.exchange.get_account()
        for res in result['balances']:
            if res['asset'] == 'BTC':
                return res['free']

    def truncate(self, f, n):
        '''Truncates/pads a float f to n decimal places without rounding'''
        s = '{}'.format(f)
        if 'e' in s or 'E' in s:
            return '{0:.{1}f}'.format(f, n)
        i, p, d = s.partition('.')
        return '.'.join([i, (d+'0'*n)[:n]])

    def run(self):
        """Docstring. Remeber to test buy amount due to way exchanges hangle fees."""
        while not self.buy_completed:
            lot = decimal.Decimal(self.exchange.get_symbol_info(symbol=self.pair)['filters'][1]['stepSize']).normalize()
            lot = lot.as_tuple().exponent
            if lot < 0:
                lot = -lot
            self.lot = lot
            self.total_btc = round(float(self.get_btc_bal()), 8)
            self.quantity = self.buy_quantity()
            try:
                self.buy_order = self.buy()
            except Exception as e:
                print(e)
                break
            self.buy_order_id = self.buy_order['orderId']
            self.buy_order_rate = self.get_buy_market()
            self.sell_order_rate = self.get_sell_market()
            if not self.config.getboolean('general', 'USE_TRAILING') and self.config.getboolean('general', 'USE_PROFIT'):
                self.take_profit = round(self.sell_order_rate + (self.sell_order_rate * self.config.getfloat('general', 'PROFITMARGIN')), 8)
            if self.config.getboolean('general', 'USE_STOPLOSS'):
                self.stoploss = round(self.sell_order_rate - (self.sell_order_rate * self.config.getfloat('general', 'STOPLOSS')), 8)
            print("=> Buy Price: ", format(self.buy_order_rate, '.8f'))
            print("=> Take Profit: ", format(self.take_profit, '.8f'))
            print("=> Stop Loss: ", format(self.stoploss, '.8f'))
            print("Bought " + self.coin + " on Binance")
            self.buy_completed = True
            self.purchase_time = datetime.now()
            time.sleep(5)
        if self.buy_completed and not self.sell_completed:
            i = 0
            while not self.sell_completed:
                current_bid = self.get_sell_market()
                upper, middle, lower = main.get_bbands(self.pair, current_bid)
                self.take_profit = middle
                if i >= 12:
                    self.progress(current_bid)
                    i = 0
                if self.trailing_stop == 0 and self.config.getboolean('general', 'USE_TRAILING'):
                    self.trailing_stop = round(current_bid + (current_bid * self.config.getfloat('general', 'TRAIL_PERCENT')), 8) + self.spread
                if current_bid >= self.trailing_stop and self.config.getboolean('general', 'USE_TRAILING'):
                    self.stoploss = round(current_bid - (current_bid * self.config.getfloat('general', 'TRAIL_STEP')), 8)
                    self.trailing_stop = round(self.sell_order_rate + (self.sell_order_rate * self.config.getfloat('general', 'TRAIL_PERCENT')), 8)
                if (
                        self.stoploss >= current_bid or
                        (datetime.now() >= (self.purchase_time + timedelta(minutes=self.config.getint('general', 'CLOSE_TIME'))) and
                         self.config.getboolean('general', 'USE_TIME_CLOSE')) or
                        (current_bid >= self.take_profit and not self.config.getboolean('general', 'USE_TRAILING') and
                         self.config.getboolean('general', 'USE_PROFIT'))
                    ):
                    self.sell_order = self.sell()
                    #self.sell_order_id = self.sell_order['orderId'] ##FIX ME##
                    print('Sell Order: ' + str(self.sell_order))
                    print('Sell Price: ', format(current_bid, '.8f'))
                    print("Sold " + self.coin + ' on Binance')
                    main.pg_sql_commit("delete from open_trades where symbol = '" + self.pair + "'")
                    self.sell_completed = True
                i += 1
                time.sleep(5)

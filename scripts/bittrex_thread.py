"""Bittrex trade thread."""
import configparser
import time
import threading
from datetime import datetime, timedelta
from bittrex.bittrex import Bittrex, API_V1_1

class BittrexThread:
    """Docstring."""
    def __init__(self, coin, pair):
        self.config = configparser.ConfigParser()
        self.config.read("config.ini")
        self.exchange = Bittrex(self.config.get('bittrex', 'key'), self.config.get('bittrex', 'secret'), api_version=API_V1_1)
        self.coin = coin
        self.pair = pair
        self.total_btc = round(float(self.exchange.get_balance('BTC')['result']['Available']), 8)
        self.quantity = self.buy_quantity()
        self.stoploss = 0
        self.take_profit = 0
        self.trailing_stop = 0
        self.purchase_time = datetime.now()
        self.buy_completed = False
        self.sell_completed = False
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def get_buy_market(self):
        """Docstring."""
        while True:
            ticks = self.exchange.get_ticker(self.pair)
            if ticks['success']:
                break
            else:
                time.sleep(5)
        return float(ticks['result']['Ask'])

    def get_sell_market(self):
        """Docstring."""
        while True:
            ticks = self.exchange.get_ticker(self.pair)
            if ticks['success']:
                break
            else:
                time.sleep(5)
        return float(ticks['result']['Bid'])

    def buy_quantity(self):
        """Docstring."""
        return round(self.trade_amount() / self.get_buy_market(), 8)

    def buy(self):
        """Docstring."""
        return self.exchange.buy_limit(self.pair, self.quantity, self.get_buy_market())

    def sell(self):
        """Docstring."""
        return self.exchange.sell_limit(self.pair, self.quantity, self.get_sell_market())

    def trade_amount(self):
        """Docstring."""
        return round(self.total_btc * self.config.getfloat('general', 'TRADE_PERCENTAGE'), 8)

    def sell_amount(self):
        """Docstring."""
        return self.quantity - (self.quantity * float(self.config.get('bittrex', 'fee')))

    def progress(self, current_bid):
        """Docstring."""
        win_loss = round((current_bid - self.sell_order_rate) / self.sell_order_rate * 100, 2)
        print("======================================")
        print("=> WAITING FOR SELL SINCE : " + str(self.purchase_time))
        print("=> BUY: " + self.pair + " @ Bittrex")
        print("=> Buy Price: ", format(self.buy_order_rate, '.8f'))
        print("=> Current Price: ", format(current_bid, '.8f'))
        print("=> Take Profit: ", format(self.take_profit, '.8f'))
        print("=> Stop Loss: ", format(self.stoploss, '.8f'))
        print("=> Win/Loss Percent: " + str(format(win_loss, '.2f')) + '%')
        print("======================================")

    def await_buy(self):
        """Docstring."""
        count = 0
        while True:
            open_order = self.exchange.get_open_orders(self.pair)['result']
            if not open_order:
                print("Bought " + self.coin + " on Bittrex")
                self.buy_completed = True
                self.purchase_time = datetime.now()
                break
            if count > self.config.getint('general', 'TIME_FOR_BUY_CANCEL'):
                buy_closed = self.exchange.cancel(self.buy_order_id)['success']
                if buy_closed:
                    break
            count = count + 1
            time.sleep(1)

    def await_sell(self, current_bid):
        """Docstring."""
        count = 0
        while True:
            open_order = self.exchange.get_open_orders(self.pair)['result']
            if not open_order:
                print('Sell Order: ' + str(self.sell_order))
                print('Sell Price: ', format(current_bid, '.8f'))
                print("Sold " + self.coin + ' on Bittrex')
                self.sell_completed = True
                break
            if count > self.config.getint('general', 'TIME_FOR_SELL_CANCEL'):
                #sell_closed = self.exchange.cancel(self.sell_order_id)['success']
                if sell_closed:
                    break
            count = count + 1
            time.sleep(1)

    def run(self):
        """Docstring. Remeber to test buy amount due to way exchanges hangle fees."""
        while not self.buy_completed:
            self.buy_order = self.buy()
            if not self.buy_order['success']:
                print(self.buy_order)
                break
            self.buy_order_id = str(self.buy_order['result']['uuid'])
            self.buy_order_rate = self.get_buy_market()
            self.sell_order_rate = self.get_sell_market()
            #float(self.exchange.get_order(self.buy_order_id)['result']['PricePerUnit'])
            if not self.config.getboolean('general', 'USE_TRAILING') and self.config.getboolean('general', 'USE_PROFIT'):
                self.take_profit = round(self.sell_order_rate + (self.sell_order_rate * self.config.getfloat('general', 'PROFITMARGIN')), 8)
            if self.config.getboolean('general', 'USE_STOPLOSS'):
                self.stoploss = round(self.sell_order_rate - (self.sell_order_rate * self.config.getfloat('general', 'STOPLOSS')), 8)
            print("=> Buy Price: ", format(self.buy_order_rate, '.8f'))
            print("=> Take Profit: ", format(self.take_profit, '.8f'))
            print("=> Stop Loss: ", format(self.stoploss, '.8f'))
            self.await_buy()
        if self.buy_completed and not self.sell_completed:
            i = 0
            while not self.sell_completed:
                current_bid = self.get_sell_market()
                if i >= 150:
                    self.progress(current_bid)
                    i = 0
                if self.trailing_stop == 0 and self.config.getboolean('general', 'USE_TRAILING'):
                    self.trailing_stop = round(current_bid + (current_bid * self.config.getfloat('general', 'TRAIL_PERCENT')), 8) + self.spread
                if current_bid >= self.trailing_stop and self.config.getboolean('general', 'USE_TRAILING'):
                    self.stoploss = round(current_bid - (current_bid * self.config.getfloat('general', 'TRAIL_STEP')), 8)
                    self.trailing_stop = round(self.sell_order_rate + (self.sell_order_rate * self.config.getfloat('general', 'TRAIL_PERCENT')), 8)
                if (
                        self.stoploss >= current_bid or
                        (datetime.now() >= (self.purchase_time + timedelta(minutes=self.config.getint('general', 'CLOSE_TIME'))) and
                         self.config.getboolean('general', 'USE_TIME_CLOSE')) or
                        (current_bid >= self.take_profit and not self.config.getboolean('general', 'USE_TRAILING') and
                         self.config.getboolean('general', 'USE_PROFIT'))
                    ):
                    self.sell_order = self.sell()
                    #self.sell_order_id = str(self.sell_order['result']['uuid']) ##FIX ME##
                    print('Sell Order: ' + str(self.sell_order))
                    print('Sell Price: ', format(current_bid, '.8f'))
                    print("Sold " + self.coin + ' on Bittrex')
                    self.sell_completed = True
                i += 1
                time.sleep(1)

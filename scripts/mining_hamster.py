#!/usr/bin/env python3
"""A simple script to take all trades
pip3 install telethon
pip3 install https://github.com/s4w3d0ff/python-poloniex/archive/v0.4.7.zip
pip3 install git+https://github.com/ericsomdahl/python-bittrex.git
pip3 install python-binance"""
import configparser
import hmac
import hashlib
import json
import time
from datetime import datetime, timedelta
import requests
from poloniex import Poloniex
from bittrex.bittrex import Bittrex, API_V1_1
import poloniex_thread
import bittrex_thread

CONFIG = configparser.ConfigParser()
CONFIG.read("config.ini")

def main():
    """Docstring."""
    key = CONFIG.get('mining_hamster', 'key')
    url = 'http://www.mininghamster.com/output/getsignal.php?apikey=' + key
    sign = hmac.new(str.encode(key), url.encode(), hashlib.sha512)
    header = {'apisign': str(sign)}
    update_handler(url, header)

def start_thread(exchange, ticker):
    """Docstring."""
    if exchange == 'poloniex':
        ticker = ticker.replace('-', '_')
        coin = ticker.replace('BTC_', '')
        print('Buying ' + ticker + ' on Poloniex')
        poloniex_thread.PoloniexThread(coin, ticker)
    if exchange == 'bittrex':
        coin = ticker.replace('BTC-', '')
        print('Buying ' + ticker + ' on Bittrex')
        bittrex_thread.BittrexThread(coin, ticker)

def update_handler(url, header):
    print('Started')
    response = json.loads(requests.get(url, headers=header).text)
    signal_time = datetime.strptime(response[0]['time'], "%Y-%m-%d %H:%M:%S")
    time.sleep(4)
    while True:
        try:
            response = json.loads(requests.get(url, headers=header).text)
        except Exception:
            time.sleep(4)
            continue
        for resp in reversed(response):
            if datetime.strptime(resp['time'], "%Y-%m-%d %H:%M:%S") > signal_time:
                start_thread(resp['exchange'], resp['market'])
                print(str(resp))
                signal_time = datetime.strptime(resp['time'], "%Y-%m-%d %H:%M:%S")
        time.sleep(4)

if __name__ == '__main__':
    main()
    
#!/usr/bin/env python3
"""A simple script to take all trades
pip3 install telethon
pip3 install https://github.com/s4w3d0ff/python-poloniex/archive/v0.4.7.zip
pip3 install git+https://github.com/ericsomdahl/python-bittrex.git
pip3 install python-binance"""
import configparser
import time
import decimal
from pyti.bollinger_bands import (upper_bollinger_band, middle_bollinger_band, lower_bollinger_band)
from pyti.relative_strength_index import relative_strength_index
from datetime import datetime, timedelta
import binance_thread
import psycopg2
from binance.client import Client
from binance.websockets import BinanceSocketManager

CONFIG = configparser.ConfigParser()
CONFIG.read("config.ini")
BIN_LIST = []

def main():
    """Docstring."""
    create_table = """create table if not exists open_trades (symbol varchar(255))"""
    pg_sql_commit(create_table)
    create_index = """create index if not exists time_index on open_trades(symbol)"""
    pg_sql_commit(create_index)
    clean_up = "delete from open_trades"
    pg_sql_commit(clean_up)

    client = Client(CONFIG.get('binance', 'key'), CONFIG.get('binance', 'secret'))

    bm = BinanceSocketManager(client)
    # start any sockets here, i.e a trade socket
    conn_key = bm.start_ticker_socket(start)
    # then start the socket manager
    print('Started')
    bm.start()

def start(msg):
    try:
        for m in msg:
            if m['s'][-3:] == 'BTC':
                asset = m['s'][:-3]
                pair = m['s']
                res = pg_sql("select symbol from open_trades")
                for r in res:
                    if pair == r[0]:
                        return
                current = float(m['a'])
                upper, middle, lower = get_bbands(pair, current)
                rsi = get_rsi(pair, current)
                if current <= lower - (lower * CONFIG.getfloat('general', 'WICK_LENGTH')) and rsi <= 30:
                    print('Buying ' + pair)
                    binance_thread.BinanceThread(asset, pair)
                    sql = "insert into open_trades (symbol) values ('" + pair + "')"
                    pg_sql_commit(sql)
    except:
        return

def pg_sql_commit(query, values=None):
    conn = psycopg2.connect("dbname='binance' user='postgres' host='localhost' password='KornDa3g'")
    if conn is not None:
        try:
            if values:
                with conn:
                    c = conn.cursor()
                    c.execute(query, values)
            else:
                with conn:
                    c = conn.cursor()
                    c.execute(query)
        except Exception as e:
            print(e)
        c.close()
        conn.close()

def get_bbands(pair, current):
    closes = get_closes(pair, 'desc')
    if closes == []:
        return
    closes = [current] + closes
    upper = upper_bollinger_band(closes, 20)
    middle = middle_bollinger_band(closes, 20)
    lower = lower_bollinger_band(closes, 20)
    try:
        upper = round([upper for upper in upper if str(upper) != 'nan'][0], 8)
        middle = round([middle for middle in middle if str(middle) != 'nan'][0], 8)
        lower = round([lower for lower in lower if str(lower) != 'nan'][0], 8)
    except Exception as e:
        print(e)
        return 0, 0, 0
    return upper, middle, lower

def get_rsi(pair, current):
    closes = get_closes(pair, 'asc')
    closes.append(current)
    rsi = relative_strength_index(closes, 14)
    try:
        rsi = round([rsi for rsi in rsi if str(rsi) != 'nan'][-1], 8)
    except Exception as e:
        print(e)
        return 100
    return rsi

def get_closes(pair, dir):
    closes = []
    sql = 'select close from binance_ticks where symbol = ' + "'" + pair + "'" 'order by end_time ' + dir
    result = pg_sql(sql)
    if result != []:
        for res in result:
            closes.append(res[0])
    return closes

def pg_sql(query, values=None):
    conn = psycopg2.connect("dbname='binance' user='postgres' host='localhost' password='KornDa3g'")
    if conn is not None:
        while True:
            try:
                if values:
                    c = conn.cursor()
                    c.execute(query, values)
                    break
                else:
                    c = conn.cursor()
                    c.execute(query)
                    break
            except Exception as e:
                    print(e)
                    time.sleep(1)
                    continue
    result = c.fetchall()
    c.close()
    conn.close()
    return result

if __name__ == '__main__':
    main()

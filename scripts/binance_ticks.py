#!/usr/bin/env python3
"""A simple script to take all trades
pip3 install telethon
pip3 install https://github.com/s4w3d0ff/python-poloniex/archive/v0.4.7.zip
pip3 install git+https://github.com/ericsomdahl/python-bittrex.git
pip3 install python-binance"""
import configparser
from time import sleep
from datetime import datetime, timedelta
import bittrex_thread
import psycopg2
from binance.client import Client
from binance.websockets import BinanceSocketManager

CONFIG = configparser.ConfigParser()
CONFIG.read("config.ini")
BIN_LIST = []
START_TIME = int(((datetime.utcnow() - datetime(1970, 1, 1, 0, 0, 0, 0)).total_seconds() + 302400) * 1000) 

def main():
    """Docstring."""
    create_table = """create table if not exists binance_ticks (start_time bigint, end_time bigint,
                    symbol varchar(255), open double precision, close double precision,
                    high double precision, low double precision, volume double precision)"""
    pg_sql(create_table)
    create_index = """create index if not exists time_index on binance_ticks (end_time)"""
    pg_sql(create_index)
    clean_up()
    client = Client(CONFIG.get('binance', 'key'), CONFIG.get('binance', 'secret'))
    bin_pairs = client.get_exchange_info()
    for pair in bin_pairs['symbols']:
        if pair['quoteAsset'] != 'BTC':
            continue
        BIN_LIST.append(pair['symbol'])
    for pair in BIN_LIST:
        bm = BinanceSocketManager(client)
        bm.start_kline_socket(pair, process_message, interval=Client.KLINE_INTERVAL_5MINUTE)
        bm.start()

def process_message(msg):
    global START_TIME
    if msg['k']['x']:
        if int(START_TIME) <= int(((datetime.utcnow() - datetime(1970, 1, 1, 0, 0, 0, 0)).total_seconds()) * 1000):
            clean_up()
            START_TIME = int(((datetime.utcnow() - datetime(1970, 1, 1, 0, 0, 0, 0)).total_seconds() + 302400) * 1000)
        sql = """insert into binance_ticks(start_time, end_time, symbol, open, close, high, 
                low, volume) values (%s,%s,%s,%s,%s,%s,%s,%s)"""
        values = (msg['k']['t'], msg['k']['T'], msg['k']['s'], msg['k']['o'], msg['k']['c'], msg['k']['h'], msg['k']['l'], msg['k']['v'])
        pg_sql(sql, values)

def pg_sql(query, values=None):
    conn = psycopg2.connect("dbname='binance' user='postgres' host='localhost' password='KornDa3g'")
    if conn is not None:
        try:
            if values:
                with conn:
                    c = conn.cursor()
                    c.execute(query, values)
            else:
                with conn:
                    c = conn.cursor()
                    c.execute(query)
        except Exception as e:
            print(e)
        c.close()
        conn.close()

def clean_up():
    day = int(((datetime.utcnow() - datetime(1970, 1, 1, 0, 0, 0, 0)).total_seconds() - 604800) * 1000)
    sql = "DELETE FROM binance_ticks WHERE end_time <= " + str(day);
    pg_sql(sql)

if __name__ == '__main__':
    main()
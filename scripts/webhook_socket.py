#!/usr/bin/env python3
"""A simple script to take all trades
pip3 install telethon
pip3 install https://github.com/s4w3d0ff/python-poloniex/archive/v0.4.7.zip
pip3 install git+https://github.com/ericsomdahl/python-bittrex.git
pip3 install python-binance"""
import configparser
import socket
import sys
import os
import json
from poloniex import Poloniex
from bittrex.bittrex import Bittrex, API_V1_1
from cryptopia_api import Api as Cryptopia
import poloniex_thread
import bittrex_thread
import cryptopia_thread

CONFIG = configparser.ConfigParser()
CONFIG.read("config.ini")
ADDR = '/tmp/david'
POLO_LIST = []
BIT_LIST = []
CRY_LIST = []

def main():
    """Docstring."""
    polo = Poloniex()
    polo_pairs = polo.returnCurrencies()
    for pair in polo_pairs:
        if pair == 'BTC':
            continue
        pair = '#' + pair
        POLO_LIST.append(pair)
    bittrex = Bittrex(None, None, api_version=API_V1_1)
    bit_pairs = bittrex.get_currencies()
    for pair in bit_pairs['result']:
        if pair['Currency'] == 'BTC':
            continue
        pair = '#' + pair['Currency']
        BIT_LIST.append(pair)
    cryptopia = Cryptopia(CONFIG.get('cryptopia', 'key'), CONFIG.get('cryptopia', 'secret'))
    cry_pairs = cryptopia.get_currencies()
    for pair in cry_pairs['Data']:
        if pair['Symbol'] == 'BTC':
            continue
        pair = '#' + pair['Symbol']
        CRY_LIST.append(pair)
    update_handler()

def start_thread(exchange, ticker):
    """Docstring."""
    if exchange == 'poloniex':
        btc = 'BTC_'
        variable = btc + ticker
        print('Buying ' + variable + ' on Poloniex')
        poloniex_thread.PoloniexThread(ticker, variable)
    if exchange == 'bittrex':
        btc = 'BTC-'
        variable = btc + ticker
        print('Buying ' + variable + ' on Bittrex')
        bittrex_thread.BittrexThread(ticker, variable)
    if exchange == 'cryptopia':
        btc = '/BTC'
        variable = ticker + btc
        print('Buying ' + variable + ' on Cryptopia')
        cryptopia_thread.CryptopiaThread(ticker, variable)

def update_handler():
    """Docstring."""
    try:
        os.unlink(ADDR)
    except OSError:
        if os.path.exists(ADDR):
            raise
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    # Bind the socket to the port
    print('Starting up on ' + ADDR)
    sock.bind(ADDR)

    # Listen for incoming connections
    sock.listen(1)

    while True:
        # Wait for a connection
        print('Waiting for a connection')
        connection, client_address = sock.accept()
        try:
            print('Connection received')

            # Receive the data in small chunks and retransmit it
            while True:
                data = connection.recv(4096)
                print('Received data')
                if data:
                    data = json.loads(data.decode())
                    start_thread(data['exchange'], data['ticker'])
                else:
                    print('No more data')
                    break
        finally:
            # Clean up the connection
            connection.close()

if __name__ == '__main__':
    main()
    

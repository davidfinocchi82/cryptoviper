""" This is a wrapper for Cryptopia.co.nz API """

import urllib
import json
import time
import hmac
import hashlib
import base64
import requests

class Api(object):
    """ Represents a wrapper for cryptopia API """

    def __init__(self, key, secret):
        self.API_KEY = key
        self.API_SECRET = secret
        self.public_set = set([ "GetCurrencies", "GetTradePairs", "GetMarkets", "GetMarket", "GetMarketHistory", "GetMarketOrders", "GetMarketOrderGroups" ])
        self.private_set = set([ "GetBalance", "GetDepositAddress", "GetOpenOrders", "GetTradeHistory", "GetTransactions", "SubmitTrade", "CancelTrade", "SubmitTip" ])

    def api_query(self, method, req = None ):
        if not req:
            req = {}
        #print "def api_query( method = " + method + ", req = " + str( req ) + " ):"
        time.sleep( 1 )
        if method in self.public_set:
            url = "https://www.cryptopia.co.nz/api/" + method
            if req:
                for key, value in req.items():
                    url += '/' + str( value )
            r = requests.get( url )
        elif method in self.private_set:
            url = "https://www.cryptopia.co.nz/Api/" + method
            nonce = str( int( time.time() ) )
            post_data = json.dumps( req );
            m = hashlib.md5()
            m.update(post_data.encode('utf-8')) 
            requestContentBase64String = base64.b64encode(m.digest()).decode('utf-8')
            signature = self.API_KEY + "POST" + urllib.parse.quote_plus( url ).lower() + nonce + requestContentBase64String
            hmacsignature = base64.b64encode(hmac.new(base64.b64decode( self.API_SECRET ), signature.encode('utf-8'), hashlib.sha256).digest())
            header_value = "amx " + self.API_KEY + ":" + hmacsignature.decode('utf-8') + ":" + nonce
            headers = { 'Authorization': header_value, 'Content-Type':'application/json; charset=utf-8' }
            r = requests.post( url, data = post_data, headers = headers )
        response = r.text
        response.replace("false","False").replace("true","True").replace('":null','":None' )
        return json.loads(response)

    def get_currencies(self):
        """ Gets all the currencies """
        return self.api_query(method='GetCurrencies')

    def get_tradepairs(self):
        """ GEts all the trade pairs """
        return self.api_query(method='GetTradePairs')

    def get_markets(self):
        """ Gets data for all markets """
        return self.api_query(method='GetMarkets')

    def get_market(self, market):
        """ Gets market data """
        return self.api_query(method='GetMarket',
                              req={'market': market})

    def get_history(self, market):
        """ Gets the full order history for the market (all users) """
        return self.api_query(method='GetMarketHistory',
                              req={'market': market})

    def get_orders(self, market):
        """ Gets the user history for the specified market """
        return self.api_query(method='GetMarketOrders',
                              req={'market': market})

    def get_ordergroups(self, markets):
        """ Gets the order groups for the specified market """
        return self.api_query(method='GetMarketOrderGroups',
                              req={'markets': markets})

    def get_balance(self, currency):
        """ Gets the balance of the user in the specified currency """
        return self.api_query(method='GetBalance', req={'Currency': currency})

    def get_openorders(self, market):
        """ Gets the open order for the user in the specified market """
        return self.api_query(method='GetOpenOrders',
                              req={'Market': market})

    def get_deposit_address(self, currency):
        """ Gets the deposit address for the specified currency """
        return self.api_query(method='GetDepositAddress',
                              req={'Currency': currency})

    def get_tradehistory(self, market, count):
        """ Gets the trade history for a market """
        return self.api_query(method='GetTradeHistory',
                              req={'Market': market, 'Count': count})

    def get_transactions(self, transaction_type):
        """ Gets all transactions for a user """
        return self.api_query(method='GetTransactions',
                              req={'Type': transaction_type})

    def submit_trade(self, market, trade_type, rate, amount):
        """ Submits a trade """
        return self.api_query(method='SubmitTrade',
                              req={'Market': market,
                                               'Type': trade_type,
                                               'Rate': rate,
                                               'Amount': amount})

    def cancel_trade(self, trade_type, tradepair_id):
        """ Cancels an active trade """
        return self.api_query(method='CancelTrade',
                              req={'Type': trade_type,
                                               'TradePairID': tradepair_id})

    def submit_tip(self, currency, active_users, amount):
        """ Submits a tip """
        return self.api_query(method='SubmitTip',
                              req={'Currency': currency,
                                               'ActiveUsers': active_users,
                                               'Amount': amount})

    def submit_withdraw(self, currency, address, amount):
        """ Submits a withdraw request """
        return self.api_query(method='SubmitWithdraw',
                              req={'Currency': currency,
                                               'Address': address,
                                               'Amount': amount})

    def submit_transfer(self, currency, username, amount):
        """ Submits a transfer """
        return self.api_query(method='SubmitTransfer',
                              req={'Currency': currency,
                                               'Username': username,
                                               'Amount': amount})
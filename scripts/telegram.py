#!/usr/bin/env python3
"""A simple script to take all trades
pip3 install telethon
pip3 install https://github.com/s4w3d0ff/python-poloniex/archive/v0.4.7.zip
pip3 install git+https://github.com/ericsomdahl/python-bittrex.git
pip3 install python-binance"""
import configparser
from getpass import getpass
from telethon import TelegramClient
from telethon.errors import SessionPasswordNeededError
from poloniex import Poloniex
from bittrex.bittrex import Bittrex, API_V1_1
from cryptopia_api import Api as Cryptopia
import poloniex_thread
import bittrex_thread
import cryptopia_thread

CONFIG = configparser.ConfigParser()
CONFIG.read("config.ini")
POLO_LIST = []
BIT_LIST = []
CRY_LIST = []

def main():
    """Docstring."""
    polo = Poloniex()
    polo_pairs = polo.returnCurrencies()
    for pair in polo_pairs:
        if pair == 'BTC':
            continue
        pair = '#' + pair
        POLO_LIST.append(pair)
    bittrex = Bittrex(None, None, api_version=API_V1_1)
    bit_pairs = bittrex.get_currencies()
    for pair in bit_pairs['result']:
        if pair['Currency'] == 'BTC':
            continue
        pair = '#' + pair['Currency']
        BIT_LIST.append(pair)
    cryptopia = Cryptopia(CONFIG.get('cryptopia', 'key'), CONFIG.get('cryptopia', 'secret'))
    cry_pairs = cryptopia.get_currencies()
    for pair in cry_pairs['Data']:
        if pair['Symbol'] == 'BTC':
            continue
        pair = '#' + pair['Symbol']
        CRY_LIST.append(pair)
    session_name = CONFIG.get('telethon', 'session_name')
    user_phone = CONFIG.get('telethon', 'phone')
    client = TelegramClient(session_name,
                            int(CONFIG.get('telethon', "api_id")),
                            CONFIG.get('telethon', 'api_hash'),
                            proxy=None,
                            update_workers=4)

    print('INFO: Connecting to Telegram Servers...')
    client.connect()
    print('Done!')
    if not client.is_user_authorized():
        print('INFO: Unauthorized user')
        client.send_code_request(user_phone)
        code_ok = False
        while not code_ok:
            code = input('Enter the auth code: ')
            try:
                code_ok = client.sign_in(user_phone, code)
            except SessionPasswordNeededError:
                password = getpass('Two step verification enabled. Please enter your password: ')
                code_ok = client.sign_in(password=password)
    print('INFO: Client initialized succesfully!')
    client.add_update_handler(update_handler)
    input('Press Enter to stop this!\n')

def start_thread(msg):
    """Docstring."""
    if 'Poloniex' in msg:
        for word in POLO_LIST:
            if word in msg:
                var = word
                var1 = var.replace('#', '')
                btc = 'BTC_'
                variable = btc + var1
                print('Buying ' + variable + ' on Poloniex')
                poloniex_thread.PoloniexThread(var1, variable)
    if 'Bittrex' in msg:
        for word in BIT_LIST:
            if word in msg:
                var = word
                var1 = var.replace('#', '')
                btc = 'BTC-'
                variable = btc + var1
                print('Buying ' + variable + ' on Bittrex')
                bittrex_thread.BittrexThread(var1, variable)
    if 'Cryptopia' in msg:
        for word in CRY_LIST:
            if word in msg:
                var = word
                var1 = var.replace('#', '')
                btc = '/BTC'
                variable = var1 + btc
                print('Buying ' + variable + ' on Cryptopia')
                cryptopia_thread.CryptopiaThread(var1, variable)

def update_handler(update):
    """Docstring."""
    try:
        msg = update.message
    except AttributeError:
        return
    msg = str(msg)
    start_thread(msg)
    print(update)
    print('Press Enter to stop this!')

if __name__ == '__main__':
    main()
    